using System.Reflection;
using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;

namespace Asp.Net.Core.Examples.KendoGrid
{
	public class Startup
	{
		#region Public
		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseRouting();
			app.UseEndpoints(endpoints => endpoints.MapDefaultControllerRoute());
		}

		public void ConfigureContainer(ContainerBuilder builder)
		{
			builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc()
					.AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
			services.AddKendo();
		}
		#endregion
	}
}
