﻿using Asp.Net.Core.Examples.KendoGrid.Infrastructure.Components.Grid.Abstractions;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;

namespace Asp.Net.Core.Examples.KendoGrid.Models
{
	/// <summary>
	/// Представляет реализацию осуществляющую создание элемента <see cref="Grid{T}" />.
	/// </summary>
	/// <seealso cref="GridBuilderBase{T}" />
	public class ExampleViewModelGridBuilder : GridBuilderBase<ExampleReportViewModel>
	{
		#region Overrided
		/// <inheritdoc />
		protected override void ConfigureColumns(GridColumnFactory<ExampleReportViewModel> columnFactory)
		{
			// Пример конфигурации для ExampleReportViewModel.

			columnFactory.Bound(model => model.Name)
						 .Title("Имя");
			columnFactory.Bound(model => model.Code)
						 .Title("Код");
		}
		#endregion
	}
}
