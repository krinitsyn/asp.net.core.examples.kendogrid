﻿namespace Asp.Net.Core.Examples.KendoGrid.Models
{
	/// <summary>
	/// Представляет пример отчета.
	/// </summary>
	/// <seealso cref="ReportViewModelBase" />
	public class ExampleReportViewModel : ReportViewModelBase
	{
		#region Properties
		public string Code { get; set; }

		public string Name { get; set; }
		#endregion
	}
}
