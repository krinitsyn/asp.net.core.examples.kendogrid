using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Asp.Net.Core.Examples.KendoGrid
{
	public static class Program
	{
		#region Public
		public static void Main(string[] args) =>
			CreateHostBuilder(args)
				.Build()
				.Run();

		#endregion

		#region Private
		private static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.UseServiceProviderFactory(new AutofacServiceProviderFactory())
				.ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());
		#endregion
	}
}
