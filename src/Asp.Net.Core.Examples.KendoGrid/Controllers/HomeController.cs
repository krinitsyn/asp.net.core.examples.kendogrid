﻿using System.Collections.Generic;
using Asp.Net.Core.Examples.KendoGrid.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;

namespace Asp.Net.Core.Examples.KendoGrid.Controllers
{
	public class HomeController : Controller
	{
		#region Public
		public IActionResult GetData([DataSourceRequest] DataSourceRequest request)
		{
			var lst = new List<ExampleReportViewModel>
			{
				new ExampleReportViewModel
				{
					Name = "Name 1",
					Code = "Code 1"
				},
				new ExampleReportViewModel
				{
					Name = "Name 2",
					Code = "Code 2"
				}
			};
			return Json(lst.ToDataSourceResult(request));
		}

		public IActionResult Index() => View();
		#endregion
	}
}
