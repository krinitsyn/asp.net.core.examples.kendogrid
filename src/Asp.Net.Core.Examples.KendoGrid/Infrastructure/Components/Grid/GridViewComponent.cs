﻿using System;
using Asp.Net.Core.Examples.KendoGrid.Infrastructure.Components.Grid.Abstractions;
using JetBrains.Annotations;
using Kendo.Mvc.UI.Fluent;
using Microsoft.AspNetCore.Mvc;

namespace Asp.Net.Core.Examples.KendoGrid.Infrastructure.Components.Grid
{
	/// <summary>
	/// Представляет компонент Таблица.
	/// </summary>
	/// <seealso cref="ViewComponent" />
	public class GridViewComponent : ViewComponent
	{
		#region Data
		#region Fields
		private readonly IGridBuilderResolver _gridBuilderResolver;
		#endregion
		#endregion

		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр класса <see cref="GridViewComponent" />.
		/// </summary>
		/// <param name="gridBuilderResolver">
		/// Реализация компонента, используемого для динамического поиска соответствующего
		/// <see cref="GridBuilder{T}" /> для типа T.
		/// </param>
		/// <exception cref="ArgumentNullException">Если <paramref name="gridBuilderResolver" /> равен null.</exception>
		public GridViewComponent([NotNull] IGridBuilderResolver gridBuilderResolver) => _gridBuilderResolver = gridBuilderResolver ?? throw new ArgumentNullException(nameof(gridBuilderResolver));
		#endregion

		#region Public
		/// <summary>
		/// Вызывается при визуализации компонента из представления.
		/// </summary>
		/// <param name="type">Тип данных, для которого необходимо построить таблицу.</param>
		/// <exception cref="ArgumentNullException">Если <paramref name="type" /> равен null.</exception>
		/// <returns>Результат для отображения представлением.</returns>
		public IViewComponentResult Invoke([NotNull] Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException(nameof(type));
			}

			var gridBuilderFunc = _gridBuilderResolver.Resolve(type);
			return View(gridBuilderFunc);
		}
		#endregion
	}
}
