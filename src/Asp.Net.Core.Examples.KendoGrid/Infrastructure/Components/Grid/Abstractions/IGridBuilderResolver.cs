﻿using System;
using Kendo.Mvc.UI;

namespace Asp.Net.Core.Examples.KendoGrid.Infrastructure.Components.Grid.Abstractions
{
	/// <summary>
	/// Описывает компонент, выполняющий поиск делегата, осуществляющего построение пользовательского элемента управления
	/// <see cref="Grid{T}" /> для конкретного типа.
	/// </summary>
	public interface IGridBuilderResolver
	{
		#region Overridable
		/// <summary>
		/// Выполняет поиск делегата, осуществляющего построение пользовательского элемента управления <see cref="Grid{T}" /> для
		/// типа <paramref name="type"></paramref>.
		/// </summary>
		/// <param name="type">Тип данных, отображаемый в пользовательском элементе управления <see cref="Grid{T}" />.</param>
		/// <returns>
		/// Делегат, осуществляющий построение пользовательского элемента управления <see cref="Grid{T}" /> для типа
		/// <paramref name="type"></paramref>.
		/// </returns>
		dynamic Resolve(Type type);
		#endregion
	}
}
