﻿using System;
using Asp.Net.Core.Examples.KendoGrid.Infrastructure.Extensions;
using Asp.Net.Core.Examples.KendoGrid.Models;
using Autofac;
using JetBrains.Annotations;
using Kendo.Mvc.UI;

namespace Asp.Net.Core.Examples.KendoGrid.Infrastructure.Components.Grid.Abstractions
{
	/// <summary>
	/// Представляет реализацию по умолчанию компонента, выполняющего поиск делегата, осуществляющего построение
	/// пользовательского элемента управления <see cref="Grid{T}" /> для конкретного типа.
	/// </summary>
	/// <seealso cref="IGridBuilderResolver" />
	public class DefaultGridBuilderResolver : IGridBuilderResolver
	{
		#region Data
		#region Fields
		private readonly IComponentContext _componentContext;
		#endregion
		#endregion

		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр класса <see cref="DefaultGridBuilderResolver" />.
		/// </summary>
		/// <param name="componentContext">Реализация компонента контекста, используемого для разрешения зависимостей.</param>
		/// <exception cref="ArgumentNullException">Если <paramref name="componentContext" /> равен null.</exception>
		public DefaultGridBuilderResolver([NotNull] IComponentContext componentContext) => _componentContext = componentContext ?? throw new ArgumentNullException(nameof(componentContext));
		#endregion

		#region IGridBuilderResolver members
		/// <summary>
		/// Выполняет поиск делегата, осуществляющего построение пользовательского элемента управления <see cref="Grid{T}" /> для
		/// типа <paramref name="type"></paramref>.
		/// </summary>
		/// <param name="type">Тип данных, отображаемый в пользовательском элементе управления <see cref="Grid{T}" />.</param>
		/// <returns>
		/// Делегат, осуществляющий построение пользовательского элемента управления <see cref="Grid{T}" /> для типа
		/// <paramref name="type"></paramref>.
		/// </returns>
		/// <exception cref="ArgumentNullException">Если <paramref name="type" /> равен null.</exception>
		/// <exception cref="ArgumentException">
		/// Если <paramref name="type" /> не является производным от типа
		/// <see cref="ReportViewModelBase" />.
		/// </exception>
		public dynamic Resolve([NotNull] Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException(nameof(type));
			}

			if (!type.IsSubtypeOfInterfaceOrClass(typeof(ReportViewModelBase)))
			{
				throw new ArgumentException($"Параметр должен быть производным от типа {nameof(ReportViewModelBase)}.", nameof(type));
			}

			var resolve = (dynamic) _componentContext.Resolve(typeof(IGridBuilder<>).MakeGenericType(type));
			return resolve.GetBuilder();
		}
		#endregion
	}
}
