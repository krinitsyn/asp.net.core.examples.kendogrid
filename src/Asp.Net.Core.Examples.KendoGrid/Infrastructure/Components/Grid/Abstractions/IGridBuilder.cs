﻿using System;
using Asp.Net.Core.Examples.KendoGrid.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;

namespace Asp.Net.Core.Examples.KendoGrid.Infrastructure.Components.Grid.Abstractions
{
	/// <summary>
	/// Описывает тип, предназначенный для создания делегата, осуществляющего построение пользовательского элемента управления
	/// <see cref="Grid{T}" /> закрытого типом <see cref="T" />.
	/// </summary>
	/// <typeparam name="T">
	/// Тип данных, для которого нужно построить пользовательский элемент управления <see cref="Grid{T}" />
	/// .
	/// </typeparam>
	public interface IGridBuilder<T> where T : ReportViewModelBase
	{
		#region Overridable
		/// <summary>
		/// Возвращает делегат, осуществляющий построение пользовательского элемента управления <see cref="Grid{T}" /> закрытого
		/// типом <see cref="T" />.
		/// </summary>
		/// <returns>
		/// Делегат, осуществляющий построение пользовательского элемента управления <see cref="Grid{T}" /> закрытого
		/// типом <see cref="T" />.
		/// </returns>
		Func<WidgetFactory<dynamic>, GridBuilder<T>> GetBuilder();
		#endregion
	}
}
