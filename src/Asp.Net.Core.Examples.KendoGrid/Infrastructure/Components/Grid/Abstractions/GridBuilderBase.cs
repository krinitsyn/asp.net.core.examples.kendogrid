﻿using System;
using Asp.Net.Core.Examples.KendoGrid.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;

namespace Asp.Net.Core.Examples.KendoGrid.Infrastructure.Components.Grid.Abstractions
{
	/// <summary>
	/// Представляет базовую реализацию типа, предназначенного для создания делегата, осуществляющего
	/// построение пользовательского элемента управления <see cref="Grid{T}"/> закрытого типом <see cref="T"/>.
	/// </summary>
	/// <typeparam name="T">Тип данных, для которого нужно построить пользовательский элемент управления <see cref="Grid{T}"/>.</typeparam>
	/// <seealso cref="IGridBuilder{T}" />
	public abstract class GridBuilderBase<T> : IGridBuilder<T> where T : ReportViewModelBase
	{
		/// <summary>
		/// Возвращает делегат, осуществляющий построение пользовательского элемента управления <see cref="Grid{T}" /> закрытого типом <see cref="!:T" />.
		/// </summary>
		/// <returns>
		/// Делегат, осуществляющий построение пользовательского элемента управления <see cref="Grid{T}" /> закрытого типом <see cref="!:T" />.
		/// </returns>
		public Func<WidgetFactory<dynamic>, GridBuilder<T>> GetBuilder()
		{
			return widgetFactory =>
			{
				if (widgetFactory == null)
				{
					throw new ArgumentNullException(nameof(widgetFactory));
				}

				var gridBuilder = widgetFactory.Grid<T>();
				ConfigureGrid(gridBuilder);
				gridBuilder.Columns(ConfigureColumns);
				return gridBuilder;
			};
		}

		/// <summary>
		/// Конфигурирует пользовательский элемент управления <see cref="Grid{T}"/>.
		/// </summary>
		/// <param name="gridBuilder">Фабрика, для конфигурирования пользовательского элемента управления <see cref="Grid{T}"/>.</param>
		protected virtual void ConfigureGrid(GridBuilder<T> gridBuilder)
		{
			// Здесь может быть какая-то общая конфигурация.
			gridBuilder.Name(typeof(T).Name)
				.Sortable()
				.Reorderable(gridReorderingSettingsBuilder => gridReorderingSettingsBuilder.Columns(true))
				.DataSource(dataSourceBuilder =>
				{
					dataSourceBuilder
						.Ajax()
						.Batch(true)
						.Read(builder => builder.Action("GetData", "Home"))
						.ServerOperation(false);
				});
		}

		/// <summary>
		/// Конфигурирует колонки в пользовательском элементе управления <see cref="Grid{T}"/>.
		/// </summary>
		/// <remarks>
		/// Набор методов в текущем классе может быть расширен при необходимсоти. Указанные методы
		/// приведены в качестве примера.
		/// </remarks>
		/// <param name="columnFactory">Фабрика, для конфигурирования колонок в пользовательском элементе управления <see cref="Grid{T}"/>.</param>
		protected abstract void ConfigureColumns(GridColumnFactory<T> columnFactory);
	}
}