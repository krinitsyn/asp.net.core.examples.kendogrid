﻿using System.Linq;
using System.Reflection;
using Asp.Net.Core.Examples.KendoGrid.Infrastructure.Components.Grid.Abstractions;
using Asp.Net.Core.Examples.KendoGrid.Infrastructure.Extensions;
using Autofac;
using JetBrains.Annotations;
using Kendo.Mvc.UI.Fluent;
using Module = Autofac.Module;

namespace Asp.Net.Core.Examples.KendoGrid.Infrastructure.IoC
{
	/// <summary>
	/// Представляет модуль для регистрации зависимостей, связанных с поиском
	/// соответствующего <see cref="GridBuilder{T}" /> для конкретного типа данных,
	/// отображаемого в таблице.
	/// </summary>
	/// <seealso cref="Autofac.Module" />
	[UsedImplicitly]
	public class GridBuilderModule : Module
	{
		#region Overrided
		/// <inheritdoc />
		protected override void Load(ContainerBuilder builder)
		{
			var assembly = Assembly.GetAssembly(typeof(IGridBuilder<>));

			if (assembly != null)
			{
				var gridBuilders = assembly.GetTypes()
										   .Where(p => p.IsSubtypeOfGenericInterface(typeof(IGridBuilder<>)))
										   .ToArray();

				builder.RegisterTypes(gridBuilders)
					   .AsImplementedInterfaces();
			}

			builder.RegisterType<DefaultGridBuilderResolver>()
				   .As<IGridBuilderResolver>();
		}
		#endregion
	}
}
