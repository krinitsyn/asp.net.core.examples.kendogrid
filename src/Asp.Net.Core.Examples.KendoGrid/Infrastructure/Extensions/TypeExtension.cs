﻿using System;
using System.Linq;
using JetBrains.Annotations;

namespace Asp.Net.Core.Examples.KendoGrid.Infrastructure.Extensions
{
	/// <summary>
	/// Содержит набор методов расширения для типа <see cref="Type" />.
	/// </summary>
	public static class TypeExtension
	{
		#region Public
		/// <summary>
		/// Определяет реализует ли целевой тип (класс, структура, интерфейс) обобщенный интерфейс.
		/// </summary>
		/// <param name="targetType">Объект целевого типа (класс, структура, интерфейс) для проверки.</param>
		/// <param name="genericInterfaceType">Тип представляющий обобщенный интерфейса.</param>
		/// <exception cref="ArgumentNullException">Если <paramref name="targetType" /> равен null.</exception>
		/// <exception cref="ArgumentNullException">Если <paramref name="genericInterfaceType" /> равен null.</exception>
		public static bool IsSubtypeOfGenericInterface([NotNull] this Type targetType, Type genericInterfaceType)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException(nameof(targetType));
			}

			if (genericInterfaceType == null)
			{
				throw new ArgumentNullException(nameof(genericInterfaceType));
			}

			return targetType.GetInterfaces()
							 .Where(p => p.IsGenericType)
							 .Select(p => p.GetGenericTypeDefinition())
							 .Contains(genericInterfaceType);
		}

		/// <summary>
		/// Определяет является ли целевой тип <see cref="targetType" /> (класс, структура, интерфейс) производным от
		/// <see cref="baseType" />.
		/// </summary>
		/// <param name="targetType">Целевой тип (класс, структура, интерфейс).</param>
		/// <param name="baseType">Базовый тип.</param>
		/// <exception cref="ArgumentNullException">Если <paramref name="targetType" /> равен null.</exception>
		/// <exception cref="ArgumentNullException">Если <paramref name="baseType" /> равен null.</exception>
		public static bool IsSubtypeOfInterfaceOrClass([NotNull] this Type targetType, Type baseType)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException(nameof(targetType));
			}

			if (baseType == null)
			{
				throw new ArgumentNullException(nameof(baseType));
			}

			if (baseType.IsInterface)
			{
				return targetType.GetInterfaces()
								 .Contains(baseType);
			}

			return baseType.IsAssignableFrom(targetType);
		}
		#endregion
	}
}
